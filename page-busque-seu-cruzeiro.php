<?php

get_header(); 
// http://qualitours.com.br/agencias/?token=9449fbed17da0224b39f0943dd5f47099989bbb9

?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg?v2" alt="">

<main class="center" role="main" ng-controller="BusqueSeuCruzeiro">
	<!-- section -->
	<section class="">

		<div class="row">
			<div class="col-sm-12">
				<h1> <?php the_title();?> </h1>


				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<?php endwhile; ?>

				<?php the_content(); ?>
				<?php //html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>


				<?php else: ?>

					<!-- article -->
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
					</article>
					<!-- /article -->

				<?php endif; ?>



			</div>
			<div class="col-sm-12">
				<iframe class="iframe_busca_cruzeiro" ng-src="{{ url_iframe }}" frameborder="0"></iframe>



			</div>



		</div>


	</section>



	<?php //get_sidebar(); ?>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/controllers/BusqueSeuCruzeiro.js"></script>
</main>



<?php get_footer(); ?>