
<?php



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$data = json_decode(file_get_contents('php://input'), true);


	$arrayCampos = array(
		'template_id' => 1,
		'to' => $data["email"],
		'subject' => $data["assunto"],
		'message' => "Nome: ".$data["nome"] . " - Telefone: " .$data["telefone"]
	);

	echo json_encode($arrayCampos);

	return;

	return send_email($arrayCampos);


}



?>

<?php get_header();?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg?v2" alt="">

<main class="center ng-cloak" role="main" ng-controller="Cias">
	<!-- section -->
	<section class="">

		<br>
		<br>		
		<div class="row">
			<div class="col-sm-12">
				<div class="sliders slider-1" >

					<?php


					if( have_rows('banners_para_a_pagina_da_cia') ):

						while ( have_rows('banners_para_a_pagina_da_cia') ) : the_row();

							?>


							<div >

								<img class="sliderImagens" src="<?php the_sub_field('imagem');?>" alt="">
							</div>

							<?php

						endwhile;

					else :

						?>

						<div >
							<img class="sliderImagens" src="https://placehold.it/1000x300&text=Nenhum%20banner" alt="">
						</div>
						<?php

					endif;

					?>



				</div>

			</div>

			<div class="col-sm-12">
				<h1><span class="bg-1"><?php  the_title(); ?></span></h1>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-xs-6">
						<br>
						<div class="form-inline">
							<div class="form-group">
								<label for="" class="cor-3">
									Compartilhar
								</label>
							</div>
							<div class="form-group redes-compartilhar">
								<a target="_blank" href="https://web.facebook.com/sharer.php?u=<?php the_permalink(); ?>">
									<i class="fa fa-facebook" style="background-color: #3d5c9f;"></i>
								</a>
								<a target="_blank" href="http://twitter.com/share?url=<?php the_permalink(); ?>">
									<i class="fa fa-twitter" style="background-color: #2aaae1;"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-6 text-right botoes-dir">
						<a data-toggle="modal" data-target="#myModal" class="pointer">
							<i class="glyphicon glyphicon-envelope"></i>
						Email</a>  | 
						<a href="javascript:window.print()">
							<i class="glyphicon glyphicon-print"></i>
							Imprimir
						</a>	
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="box-diferenciais">
					<h2>Diferenciais:</h2>
					<?php echo get_field("diferenciais"); ?>
				</div>
				<div class="mais-textos">
					<?php echo get_field("texto_abaixo_diferenciais"); ?>
				</div>
			</div>
			<div class="col-sm-12 ">
				<hr>

				<div class="row box-frota">
					<div class="col-sm-4 padding-0 text-center no-print">
						<img src="<?php echo get_field("imagem_destaque_da_frota"); ?>" alt="">
					</div>
					<div class="col-sm-8 padding-0">
						<h2>Frota</h2>
						<?php echo get_field("texto_descritivo_da_frota"); ?>
						<ul class="lista-navios">
							<?php


							if( have_rows('lista_de_navios') ):

								while ( have_rows('lista_de_navios') ) : the_row();

									?>


									<li> <?php the_sub_field("nome_do_navio_item"); ?> </li>

									<?php

								endwhile;


							endif;

							?>
						</ul>
					</div>
				</div>

				<hr>

			</div>
			<div class="col-sm-12 no-print">
				<?php if( have_rows('locations') ): ?>
					<div class="acf-map">
						<?php while ( have_rows('locations') ) : the_row(); 

							$location = get_sub_field('location');

							?>
							<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
								<h4><?php the_sub_field('title'); ?></h4>
								<p class="address"><?php echo $location['address']; ?></p>
								<p><?php the_sub_field('description'); ?></p>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-sm-12 box-destinos">
				<h2>Destinos</h2>
				<ul class="lista-navios">
					<?php


					if( have_rows('destinos') ):

						while ( have_rows('destinos') ) : the_row();
							$data = get_sub_field("data");
							if ($data) {
								$data = "(".$data.")";
							}
							?>


							<li> <?php the_sub_field("destino"); ?>  <?php echo $data;  ?>   </li>

							<?php

						endwhile;


					endif;

					?>
				</ul>
			</div>
			<div class="col-sm-12 text-center">
				<br>
				<br>
				<a  data-toggle="modal" data-target="#modal-cotacao" class="btn botao-1 pointer">CLIQUE AQUI E SOLICITE JÁ!</a>
			</div>
		</div>




	</section>


	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header hide">
					<h3 class="modal-title">Revecer no email</h3>
				</div>
				<div class="modal-body">
					<button type="button" class="close pull-right" data-dismiss="modal"><i class="fa fa-close"></i></button>
					<div class="row">
						<div class="col-sm-12">
							
							<div class="form-group">
								<label for="email2">Email</label>
								<input id="email2" type="email" class="form-control">
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer hide">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<!-- Solicitar cotacao -->
	<div id="modal-cotacao" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close " data-dismiss="modal"><i class="fa fa-close"></i></button>
					<h3 class="modal-title">Solicitar atendimento</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<form novalidate name="formAtendimento">
								<div class="form-group" ng-class="{'has-error' : formAtendimento.nome.$invalid && formAtendimento.$submitted}">
									<label for="assunto">Nome Completo*</label>
									<input required id="nome" ng-model="atendimento.nome" name="nome" type="text" class="form-control">
									<span class="text-danger" ng-show="formAtendimento.nome.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>						

								<div class="form-group" ng-class="{'has-error' : formAtendimento.email.$invalid && formAtendimento.$submitted}">
									<label for="email2">Email*</label>
									<input required ng-model="atendimento.email" name="email" id="email2" type="email" class="form-control">
									<span class="text-danger" ng-show="formAtendimento.email.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>

								<div class="form-group" ng-class="{'has-error' : formAtendimento.telefone.$invalid && formAtendimento.$submitted}">
									<label for="tel">Telefone*</label>
									<input required id="tel" ng-model="atendimento.telefone" name="telefone" type="text" class="form-control tel" placeholder="(00) 00000-0000">
									<span class="text-danger" ng-show="formAtendimento.telefone.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>							

								<div class="form-group" ng-class="{'has-error' : formAtendimento.assunto.$invalid && formAtendimento.$submitted}">
									<label for="assunto">Assunto*</label>
									<input required id="assunto" ng-model="atendimento.assunto" value="<?php echo 'teste';?>" name="assunto" type="text" class="form-control">
									<span class="text-danger" ng-show="formAtendimento.assunto.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>

								<div class="alert alert-danger alert-dismissible" ng-show="error">
									<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-close"></i>&times;</a>
									<strong>Aviso!</strong> {{ errorMsg }}
								</div>
								<div class="alert alert-success alert-dismissible" ng-show="sucess">
									<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-close"></i>&times;</a>
									{{ sucessMsg }}
								</div>

								<div class="fom-group">
									<span class="text-danger" ng-hide="formAtendimento.$valid" >* Preencher todos campos obrigatórios.</span>
									<button type="submit" ng-disabled="formAtendimento.email.$invalid" ng-click="enviarEmail(formAtendimento.$valid)" class="btn btn-success pull-right" >Solicitar atendimento</button>
								</div>

							</form>
						</div>
					</div>
				</div>
				<div class="hide modal-footer">
				</div>
			</div>

		</div>
	</div>

	<script>

		(function ($, root, undefined) {
			$(".slider-1").slick({
				slidesToShow: 1, 
				infinite: true, 
				autoplay: true, 
				fade : true, 
				speed: 300, 
				cssEase:"linear", 
				adaptiveHeight: true, 
				prevArrow: '<button type="button" class="my-slick-prev"><span class="glyphicon glyphicon-menu-left"></button>',
				nextArrow: '<button type="button" class="my-slick-next"><span class="glyphicon glyphicon-menu-right"></button>',
				dots:false
			}); 
		})(jQuery, this);




	</script>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/controllers/Cias.js"></script>
</main>



<style type="text/css">

.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin: 20px 0;
}

/* fixes potential theme css conflict */
.acf-map img {
	max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNn7qfv2akSTpbZXPKGL5vXOSB3Pah0TY"></script>


<script type="text/javascript">
	(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
		add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
		map.setCenter( bounds.getCenter() );
		map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>



<?php get_footer(); ?>