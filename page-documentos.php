<?php get_header(); ?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">

<main class="center" role="main">
	<!-- section -->
	<section>

		<h1><?php the_title(); ?></h1>

		<div class="row">
			<div class="col-sm-12">
				
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>


					<?php the_content(); ?>



				<?php endwhile; endif; ?>
			</div>


		</div>

		<?php 

		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		query_posts( array(
			'post_type'=> 'portifolio',
			'category_name'  => 'documentacao',
			'posts_per_page' => 12,
			'paged' => $paged,
			'order' => 'ASC',
		) ); 

		?>
		<?php get_template_part('loop-style-2'); ?>

	</section>

</main>


<?php get_footer(); ?>