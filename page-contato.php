<?php get_header(); 
global $post;
?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg?v2" alt="">



<main class="center" role="main">

	<section>



		<div class="row">
			<div class="col-sm-6">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-sm-12">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<?php the_content();   ?>

				<?php endwhile; endif; ?>
			</div>
			<div class="col-sm-6 pull-right">
				<div class="paginacao-estilo1">
					<?php get_template_part('pagination'); ?>
				</div>
			</div>

		</div>

		<br class="clear">

		<?php edit_post_link(); ?>

	</section>

	<?php //get_sidebar(); ?>
</main>



<?php get_footer(); ?>
