<div class="box-elems-paginacao box-estilo-doc-or-news box-estilo-resultados-pesquisas">

	<?php posts_nav_link(' ','<div class="btn-pag btn-page-prev">prev</div>','<div class="btn-pag btn-page-next">next</div>'); ?>

	<div class="row">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<div class="col-sm-3">

				<a class="links-results" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<h3><?php the_title(); ?></h3>
					<p class="data text-uppercase"><?php the_time('d M Y'); ?></p>
				</a>

			</div>

		<?php endwhile; ?>

	</div>
	<div class="row">

		<div class="col-sm-12">
			<div class="paginacao-estilo1">
				<?php get_template_part('pagination'); ?>
			</div>
		</div>
	</div>
</div>

<?php else: ?>

	<!-- article -->
	<article>
		<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
		<a href="<?php echo home_url("/");?>">< Voltar</a>
	</article>
	<!-- /article -->

<?php endif; ?>
