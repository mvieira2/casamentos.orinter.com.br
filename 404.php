<?php get_header(); ?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">

	<main class="center" role="main">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
		<?php get_sidebar(); ?>
	</main>



<?php get_footer(); ?>
