<?php get_header(); ?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">

<main class="center" role="main">
	<!-- section -->
	<section class="">

		<div class="row">
			<div class="col-sm-12 text-left">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-sm-12">

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>


					<?php the_content(); ?>



				<?php endwhile; endif; ?>
			</div>


			<div class="col-sm-12 ">
				
				<div class="lista-alinhada" style="width: 100%;"> 


					<div class="row">

						<?php


						$config = array( 
							'post_type' => 'page',
							'posts_per_page' => -1,
							'order' => 'ASC',
						) ;

						$descendants = new WP_Query( $config );
						$excl = '';

						if ( $descendants->have_posts() ) {

							while ( $descendants->have_posts() ) {
								$descendants->the_post();
								if (has_category( "lgbti", $descendants->post->ID) ) {
									$excl .= $descendants->post->ID . ",";
								}
							}

							wp_reset_postdata();
						}

						$children = get_pages(array(
							'child_of' => get_page_by_path("destinos")->ID, 
							'parent' => get_page_by_path("destinos")->ID,
							//'exclude'  => $excl,
						));


						$colunas = 0;
						$media_num_colunas = round( count($children) / 4  );
						$media_num_colunas = max(4, $media_num_colunas);

						if (get_field('limite_de_paises_por_coluna')) {
							$media_num_colunas = get_field('limite_de_paises_por_coluna');
						}	

						$limite_linha = 8;


						foreach ($children as $key => $child) { 
							$colunas = $colunas + 1;

							$children_1 = get_pages(array(
								'child_of' => $child->ID, 
								'parent' => $child->ID,
								//'exclude'  => $excl,
							));

							if (count($children_1) > 0) {

								?>

								<?php if ($colunas == 1 or count($children_1) > $limite_linha ) { ?>
									
									<?php if ($colunas > 1 && count($children_1) > $limite_linha ) { ?>

									</div>

								<?php } ?>

								<div class="col-sm-3">

								<?php } ?>

								<h2 class="title_lista"><?php echo $child->post_title ; ?></h2>

								<ul class="children">


									<?php

									foreach ($children_1 as $child_1) { 
										?>
										<li class="page_item">
											<a href="<?php echo get_permalink($child_1->ID); ?>"><?php echo $child_1->post_title; ?></a>
										</li>

									<?php } ?>

								</ul>


								<?php if ($colunas == $media_num_colunas or count($children_1) > $limite_linha ) { ?>
								</div>
								<?php 
								$colunas = 0;
							}; 
						}; 

					}; ?>

				</div>



			</div>	

		</div>

	</div>


</section>
<!-- /section -->

<?php //get_sidebar(); ?>
</main>



<?php get_footer(); ?>