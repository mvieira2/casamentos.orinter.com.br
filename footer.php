
<footer class="footer" role="contentinfo">

	<div class="center text-center">

		<div class="redes">

			<h4 class="cor-1 pull-left">
				ACOMPANHE
			</h4>

			<a href="https://pt-br.facebook.com/Orinter" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/facebook.png" alt="facebook">
			</a>
			<a href="https://twitter.com/orinter" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/twitter.png" alt="twitter">
			</a>
			<a href="https://www.instagram.com/orintertt/" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/instagram.png" alt="instagram">
			</a>
			<a href="https://www.youtube.com/user/GrupoOrinter" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/youtube.png" alt="youtube">
			</a>
		</div>
		<div class="clearfix"></div>
		<p class="cor-1">
			
			TOLL FREE: <a class="cor-1 font-destaque" href="tel:0800 449 1008">0800 449 1008</a> | E-MAIL: <a class="cor-1 font-destaque" href="mailto:casamentos@orinter.com.br">casamentos@orinter.com.br</a>

		</p>



		<p class="text-center">
			Comunicação válida apenas para profissionais do turismo. <br>
			Copyright 2018 All rights reserved. Orinter Tour & Travel

		</p>
		<p class=" text-center copyright">
			<?php _e('Powered by', 'html5blank'); ?>
			<a href="//wpw.com.br"  target="_blank" title="wpw.com.br">wpw.com.br</a>
		</p>

	</div>



</footer>

<script>
	jQuery().ready(function () {
		var links = jQuery("a");
		var urlUsadaLink = "52.67.12.73/weddings";
		var urlUsadaLink2 = "weddings.orinter.com.br";
		var urlPadrao = "casamentos.orinter.com.br"


		for (var i = 0; i < links.length; i++) {
			if (links.eq(i).attr("href").indexOf(urlUsadaLink) > -1) {
				links.eq(i).attr("href", links.eq(i).attr("href").replace( urlUsadaLink, urlPadrao ) )
			}
			if (links.eq(i).attr("href").indexOf(urlUsadaLink2) > -1) {
				links.eq(i).attr("href", links.eq(i).attr("href").replace( urlUsadaLink2, urlPadrao ) )
			}


		}



	})

</script>



<?php wp_footer(); ?>



</body>
</html>
