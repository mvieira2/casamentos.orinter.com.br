<?php get_header(); ?>
<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">
	<main class="center" role="main">
		<!-- section -->
		<section>

			<h1><?php _e( 'Categories for ', 'html5blank' ); single_cat_title(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
		<?php get_sidebar(); ?>
	</main>



<?php get_footer(); ?>
