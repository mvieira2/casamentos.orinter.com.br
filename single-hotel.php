<?php get_header();?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg?v2" alt="">

	<main class="center" role="main">
		<!-- section -->
		<section class="">

			<br>
			<br>		
			<div class="row">
				<div class="col-sm-12">
					<div class="sliders slider-1" >

						<?php


						if( have_rows('banners_para_a_pagina_da_cia') ):

							while ( have_rows('banners_para_a_pagina_da_cia') ) : the_row();

								?>


								<div >

									<img class="sliderImagens" src="<?php the_sub_field('imagem');?>" alt="">
								</div>

								<?php

							endwhile;

						else :

							?>

							<div >
								<img class="sliderImagens" src="https://placehold.it/1000x300&text=Nenhum%20banner" alt="">
							</div>
							<?php

						endif;

						?>



					</div>

				</div>

				<div class="col-sm-12">
					<h1><span class="bg-1"><?php  the_title(); ?></span></h1>
				</div>
				<div class="col-sm-12">
					<div class="row no-print">
						<div class="col-xs-6">
							<br>
							<div class="form-inline">
								<div class="form-group">
									<label for="" class="cor-3">
										Compartilhar
									</label>
								</div>
								<div class="form-group redes-compartilhar">
									<a target="_blank" href="https://web.facebook.com/sharer.php?u=<?php the_permalink(); ?>">
										<i class="fa fa-facebook" style="background-color: #3d5c9f;"></i>
									</a>
									<a target="_blank" href="http://twitter.com/share?url=<?php the_permalink(); ?>">
										<i class="fa fa-twitter" style="background-color: #2aaae1;"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-xs-6 text-right botoes-dir">
							<a data-toggle="modal" data-target="#myModal" class="pointer">
								<i class="glyphicon glyphicon-envelope"></i>
							Email</a>  | 
							<a onclick="window.print()" class="pointer">
								<i class="glyphicon glyphicon-print"></i>
								Imprimir
							</a>	
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="box-diferenciais">
						<?php echo get_field("texto_destaque"); ?>
					</div>
				</div>
				<div class="col-sm-12 ">
					<div class="row box-frota">
						<div class="col-sm-12">

							<div class="panel-group" id="accordion">


								<?php


								if( have_rows('lista') ):


									while ( have_rows('lista') ) : the_row();
										//the_sub_field('titulo');

										?>

										<div class="panel panel-default">
											<div class="panel-heading pointer <?php echo get_row_index() == 1 ? 'initenable_DISABLED' : ''; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo get_row_index(); ?>">
												<span class="pull-right btn-ocultar">OCULTAR <i class="glyphicon glyphicon-menu-up"></i> </span>
												<span class="pull-right btn-mostrar">MOSTRAR <i class="glyphicon glyphicon-menu-down"></i> </span>
												<h4 class="panel-title">
													<?php the_sub_field('titulo'); ?>
												</h4>
											</div>
											<div id="collapse_<?php echo get_row_index(); ?>" class="panel-collapse collapse <?php echo get_row_index() == 1 ? 'in_DISABLED' : ''; ?> ">
												<div class="panel-body">
													<?php the_sub_field('conteudo');?>
												</div>
											</div>
										</div>

										<?php

									endwhile;

								else :



								endif;

								?>



							</div>


						</div>
					</div>

				</div>

				<div class="col-sm-12 text-center">
					<div class="form-solicitar formulario_contato">
						<h2 class="text-left">Solicitar: </h2>
						<?php echo do_shortcode( '[contact-form-7 id="277" title="Formulário de contato Destinos"]' );?>
					</div>
				</div>
			</div>




		</section>


		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header hide">
						<h3 class="modal-title">Receber no email</h3>
					</div>
					<div class="modal-body enviar-roteiro">
						<button type="button" class="close pull-right" data-dismiss="modal"><i class="fa fa-close"></i></button>
						<div class="row">
							<div class="col-sm-12">

								<?php echo do_shortcode( '[contact-form-7 your-name="teste" title="Send email"]' ); ?>
								<script>
									init()

									function init(){
										setTimeout(function(){

											if (!jQuery(".wpcf7").length) {
												init();
												return
											}

											jQuery('.enviar-roteiro [name="your-subject"]').val('<?php the_title(); ?>');
											//jQuery('[name="your-image"]').val('<?php echo get_field("banners_para_a_pagina_da_cia")[0]["imagem"]; ?>')
											jQuery('.enviar-roteiro [name="your-message"]').val(jQuery(".box-frota").text().replace(/MOSTRAR/ig, ' ').replace(/OCULTAR/ig, ' '))

											//alert("carregou form")

											jQuery('.formulario_contato [name="hotel"]').val('<?php the_title(); ?>');

										},500);

									}

									document.addEventListener( 'wpcf7submit', function( event ) {
										//var inputs = event.detail.inputs;


										
									}, false );

								</script>

							</div>
						</div>
					</div>
					<div class="modal-footer hide">

					</div>
				</div>

			</div>

		</div>



		<script>

			(function ($, root, undefined) {
				$(".slider-1").slick({
					slidesToShow: 1, 
					infinite: true, 
					autoplay: true, 
					fade : true, 
					speed: 300, 
					cssEase:"linear", 
					adaptiveHeight: true, 
					prevArrow: '<button type="button" class="my-slick-prev"><span class="glyphicon glyphicon-menu-left"></button>',
					nextArrow: '<button type="button" class="my-slick-next"><span class="glyphicon glyphicon-menu-right"></button>',
					dots:false
				}); 
			})(jQuery, this);




		</script>
	</main>


<?php get_footer(); ?>