var WS = {
	API: 'http://api.hotshopgroup.com/',
	WEBAPI: 'http://api.hotshopgroup.com/webapi/',
	SIHOS: 'http://sihos.portalhsol.com.br/',
	Sair: function() {
		window.localStorage.clear();
		window.location.assign('/')
	},
	Post: function(url, callback, data) {
		
		var params = {
			type: "POST",
			crossDomain:true,
			dataType : "json",
			contentType:"application/x-www-form-urlencoded; charset=utf-8",		
			url: url,
		}

		if (data)
			params.data = data;	

		if (callback)
			params.success = callback;

		$.ajax(params);
	}
};

var Utils = {
	IconeLoaderOn: function(){ 
		jQuery('#ajax-loading-screen').css({"opacity":"1", "display":"block", "background":"#ffffff80"});
		jQuery('#ajax-loading-screen .loading-icon ').css({"opacity":"1"});
	},
	IconeLoadeOff: function(){ 
		jQuery('#ajax-loading-screen').css({"opacity":"0", "display":"none"});
		jQuery('#ajax-loading-screen .loading-icon ').css({"opacity":"0"});
	},
	SomenteNumeros: function(num) {
		var expCPF = /[^\d]/g;
		return num.replace(expCPF, '');
	},
	Masks: function() {

		jQuery('.data').unmask().mask('00/00/0000');
		jQuery('.cep').unmask().mask('00000-000');
		jQuery('.cnpj').unmask().mask('00.000.000/0000-00');
		jQuery('.cpf').unmask().mask('000.000.000-00');
		jQuery('.decimal').unmask().mask('#.##0,00', {reverse: true});
		jQuery('.moedaComVirgula').unmask().mask('#.##0,00', {reverse: true});
		jQuery('.moeda').unmask().mask('###0.00', {reverse: true});
		jQuery('.pontos').unmask().mask('000.000.000.000.000.00');
		// jQuery('.inteiro').text( jQuery(this).text().replace('.00',''));

		jQuery('.data').datepicker({
			language: "pt-BR",
			autoclose: true
		});
		jQuery('.input-daterange').datepicker({
			language: "pt-BR",
			autoclose: true
		});


		var SPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(SPMaskBehavior.apply({}, arguments), options);
			}
		};
		jQuery('.tel').unmask().mask(SPMaskBehavior, spOptions);
	}
}

