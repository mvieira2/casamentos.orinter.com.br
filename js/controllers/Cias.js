meuApp.controller("Cias", function($log, $scope, $rootScope, API, $timeout, $window, $http, $exceptionHandler){

	Utils.Masks();

	$scope.enviarEmail = async function(isValid){

		if (!isValid) {
			API.alertar_error('Favor preencher os campos obrigatórios');
			return
		}

		let atendimentoModel = {
			action: 'send_email',
			nonce: ajax_params.ajax_nonce,
			nome:'joao',
			email: 'mvieira@hsolmkt.com.br',
			mensagem:'mensagem teste'
		}



		let data = await API.post(ajax_params.ajax_url, atendimentoModel);
	

		if (!data.error) {
			API.alertar("Email enviado com sucesso!");
			return;
		}



	}



	$scope.goUrl =function(url, paramsUrl){
		$window.location.href = url + API.b64EncodeUnicode(paramsUrl);
	}


});

