//Utils.Masks();

var meuApp = angular.module("meuApp", [] );

meuApp.config(function ($httpProvider) {
	$httpProvider.interceptors.push(function ($q, $rootScope) {
		return {
			'request': function(config) {
				$rootScope.loading = true;
				//config.url = config.url +'?temp=' + new Date().getTime()
				return config;
			},
			'requestError': function(rejection) {
				$rootScope.loading = false;
				return $q.reject(rejection);
			},
			'response': function(response) {
				$rootScope.loading = false;
				return response;
			},
			'responseError': function(rejection) {
				$rootScope.loading = false;
				return $q.reject(rejection);
			}
		};

	});
}).factory("API", function($http, $rootScope, $timeout){

	var api = {
		alertar : function (msg) {
			$rootScope.sucess=true;
			$rootScope.sucessMsg=msg;

			$timeout( function(){
				$rootScope.sucess=false;
			}, 3000 );

		},
		alertar_error :  function (msg) {
			$rootScope.error=true;
			$rootScope.errorMsg=msg;
			$timeout( function(){
				$rootScope.error=false;
			}, 5000 );

		},
		call : function (chamada, metodo,parametros ) {
			Utils.IconeLoaderOn();

			let parent = this;

			return $http({
				url: chamada,
				method: (metodo||'POST'),
				cache: false,
				type: 'json',
				params:(parametros||null)
			}).then(function (r) {

				console.log(chamada+'\n')
				console.log(r.data)

				Utils.IconeLoadeOff()

				return {error : false, data : r.data};

			}, function (error) {
				parent.alertar_error( error.statusText  );
				return { error: true, message: error.statusText };
			});
		},

	}

	return {
		solicitar : api.call,
		get : function(chamada, opts){
			return api.call(chamada, 'GET', opts);
		},
		post : function(chamada,opts){
			return api.call(chamada, 'POST', opts);
		},
		get_params_url: function (param, url) {
			var url = new URL(url||window.location.href);
			return url.searchParams.get(param);
		},
		b64EncodeUnicode : function (str) {
			return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
				function toSolidBytes(match, p1) {
					return String.fromCharCode('0x' + p1);
				})).replace('==','');
		},
		b64DecodeUnicode : function (str) {
			return decodeURIComponent(atob(str).split('').map(function(c) {
				return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
			}).join(''));
		},		
		alertar: api.alertar,
		alertar_error: api.alertar_error	
	};

})
