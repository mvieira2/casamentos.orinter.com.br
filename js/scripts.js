(function ($, root, undefined) {

	$(document).ready(function(){


		init_sombra();

		$(window).scroll(function(){
			if( $(window).width() > 959 ){
				var cur_pos = jQuery(this).scrollTop();
				

				if( cur_pos > 50 ){
					$(".header").addClass("header-fixo");
				}else{
					$(".header").removeClass("header-fixo");
				}
			}	
			
		})	

		$(window).resize(function(){
			init_sombra();
		});


		
	})


	function init_sombra (){


		let menuActive = $(".navbar-nav > li.active");
		let menuHover = $(".navbar-nav>li");



		if (menuActive.length > 0) {
			left_pos= menuActive.position().left - parseInt(menuActive.css('padding-left'));
			bar_width = menuActive.width();

		}
		else {
			left_pos = 0;
			bar_width = 0;
		}


		$(".sombra-links").animate({
			'width': bar_width, 'left': left_pos 
		},
		{ duration: 200, queue: false }
		);


		menuHover.hover(function(){
			left_pos1= $(this).position().left;
			bar_width1 = $(this).width()

			$(".sombra-links").animate({
				'width': bar_width1, 'left': left_pos1
			},
			{ duration: 200, queue: false }
			);

		},function(){

			$(".sombra-links").animate({
				'width': bar_width, 'left': left_pos
			},
			{ duration: 200, queue: false }
			);

		})
	}




})(jQuery, this);
