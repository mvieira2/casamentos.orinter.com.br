<?php get_header();?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg?v2" alt="">

<main class="center ng-cloak" role="main" ng-controller="Cias">
	<!-- section -->
	<section class="">

		<br>
		<br>		
		<div class="row">
			<div class="col-sm-12">
				<div class="sliders slider-1" >

					<?php


					if( have_rows('banners_para_a_pagina_da_cia') ):

						while ( have_rows('banners_para_a_pagina_da_cia') ) : the_row();

							?>


							<div >

								<img class="sliderImagens" src="<?php the_sub_field('imagem');?>" alt="">
							</div>

							<?php

						endwhile;

					else :

						?>

						<div >
							<img class="sliderImagens" src="https://placehold.it/1000x300&text=Nenhum%20banner" alt="">
						</div>
						<?php

					endif;

					?>



				</div>

			</div>

			<div class="col-sm-12">
				<h1><span class="bg-1"><?php  the_title(); ?></span></h1>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-xs-6">
						<br>
						<div class="form-inline">
							<div class="form-group">
								<label for="" class="cor-3">
									Compartilhar
								</label>
							</div>
							<div class="form-group redes-compartilhar">
								<a target="_blank" href="https://web.facebook.com/sharer.php?u=<?php the_permalink(); ?>">
									<i class="fa fa-facebook" style="background-color: #3d5c9f;"></i>
								</a>
								<a target="_blank" href="http://twitter.com/share?url=<?php the_permalink(); ?>">
									<i class="fa fa-twitter" style="background-color: #2aaae1;"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-xs-6 text-right botoes-dir">
						<a data-toggle="modal" data-target="#myModal" class="pointer">
							<i class="glyphicon glyphicon-envelope"></i>
						Email</a>  | 
						<a onclick="window.print()" class="pointer">
							<i class="glyphicon glyphicon-print"></i>
							Imprimir
						</a>	
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="box-diferenciais">
					<?php echo get_field("texto_destaque"); ?>
				</div>
			</div>
			<div class="col-sm-12 ">
				<div class="row box-frota">
					<div class="col-sm-12">
						<?php echo get_field("conteudo"); ?>
					</div>
				</div>

			</div>

			<div class="col-sm-12 text-center">
				<br>
				<hr>
				<a ng-click="goUrl('<?php echo home_url('index.php/busque-seu-cruzeiro') .'?url=' ; ?>','<?php echo get_field("url_do_iframe"); ?>' )" class="btn botao-1 pointer">
					CLIQUE AQUI E RESERVE JÁ
				</a>
			</div>
		</div>




	</section>


	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header hide">
						<h3 class="modal-title">Receber no email</h3>
					</div>
					<div class="modal-body">
						<button type="button" class="close pull-right" data-dismiss="modal"><i class="fa fa-close"></i></button>
						<div class="row">
							<div class="col-sm-12">

								<?php echo do_shortcode( '[contact-form-7 your-name="teste" title="Send email"]' ); ?>
								<script>
									init()

									function init(){
										setTimeout(function(){

											if (!jQuery(".wpcf7").length) {
												init();
												return
											}

											jQuery('[name="your-subject"]').val('<?php the_title(); ?>')
											jQuery('[name="your-image"]').val('<?php echo get_field("banners_para_a_pagina_da_cia")[0]["imagem"]; ?>')
											jQuery('[name="your-message"]').val(jQuery(".box-frota").text())

											//alert("carregou form")


										},500);

									}

									document.addEventListener( 'wpcf7submit', function( event ) {
										//var inputs = event.detail.inputs;

						
										
									}, false );

								</script>

							</div>
						</div>
					</div>
					<div class="modal-footer hide">
					
					</div>
				</div>

			</div>

	</div>

	<!-- Solicitar cotacao -->
	<div id="modal-cotacao" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close " data-dismiss="modal"><i class="fa fa-close"></i></button>
					<h3 class="modal-title">Solicitar atendimento</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<form novalidate name="formAtendimento">
								<div class="form-group" ng-class="{'has-error' : formAtendimento.nome.$invalid && formAtendimento.$submitted}">
									<label for="assunto">Nome Completo*</label>
									<input required id="nome" ng-model="atendimento.nome" name="nome" type="text" class="form-control">
									<span class="text-danger" ng-show="formAtendimento.nome.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>						

								<div class="form-group" ng-class="{'has-error' : formAtendimento.email.$invalid && formAtendimento.$submitted}">
									<label for="email2">Email*</label>
									<input required ng-model="atendimento.email" name="email" id="email2" type="email" class="form-control">
									<span class="text-danger" ng-show="formAtendimento.email.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>

								<div class="form-group" ng-class="{'has-error' : formAtendimento.telefone.$invalid && formAtendimento.$submitted}">
									<label for="tel">Telefone*</label>
									<input required id="tel" ng-model="atendimento.telefone" name="telefone" type="text" class="form-control tel" placeholder="(00) 00000-0000">
									<span class="text-danger" ng-show="formAtendimento.telefone.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>							

								<div class="form-group" ng-class="{'has-error' : formAtendimento.assunto.$invalid && formAtendimento.$submitted}">
									<label for="assunto">Assunto*</label>
									<input required id="assunto" ng-model="atendimento.assunto" value="<?php echo 'teste';?>" name="assunto" type="text" class="form-control">
									<span class="text-danger" ng-show="formAtendimento.assunto.$invalid && formAtendimento.$submitted">* Campo obrigatório</span>
								</div>

								<div class="alert alert-danger alert-dismissible" ng-show="error">
									<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-close"></i>&times;</a>
									<strong>Aviso!</strong> {{ errorMsg }}
								</div>
								<div class="alert alert-success alert-dismissible" ng-show="sucess">
									<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-close"></i>&times;</a>
									{{ sucessMsg }}
								</div>

								<div class="fom-group">
									<span class="text-danger" ng-hide="formAtendimento.$valid" >* Preencher todos campos obrigatórios.</span>
									<button type="submit" ng-disabled="formAtendimento.email.$invalid" ng-click="enviarEmail(formAtendimento.$valid)" class="btn btn-success pull-right" >Solicitar atendimento</button>
								</div>

							</form>
						</div>
					</div>
				</div>
				<div class="hide modal-footer">
				</div>
			</div>

		</div>
	</div>

	<script>

		(function ($, root, undefined) {
			$(".slider-1").slick({
				slidesToShow: 1, 
				infinite: true, 
				autoplay: true, 
				fade : true, 
				speed: 300, 
				cssEase:"linear", 
				adaptiveHeight: true, 
				prevArrow: '<button type="button" class="my-slick-prev"><span class="glyphicon glyphicon-menu-left"></button>',
				nextArrow: '<button type="button" class="my-slick-next"><span class="glyphicon glyphicon-menu-right"></button>',
				dots:false
			}); 
		})(jQuery, this);




	</script>
	<script src="<?php bloginfo( 'template_url' ); ?>/js/controllers/Cias.js"></script>
</main>




<?php get_footer(); ?>