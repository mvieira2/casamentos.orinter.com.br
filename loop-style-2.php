<div class="box-elems-paginacao box-estilo-doc-or-news">

	<?php posts_nav_link(' ','<div class="btn-pag btn-page-prev">prev</div>','<div class="btn-pag btn-page-next">next</div>'); ?>

	<div class="row">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<div class="col-sm-3">
				<?php if(get_field('website_url')) { ?>
					<a target="_blank" href="<?php echo get_field('website_url'); ?>">
				<?php } else { ?>
					<a target="_blank" href="<?php echo get_the_post_thumbnail_url(); ?>" >
				<?php } ?>
					<div class="box-img-desc logo-bg" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>)">
						<div class="over-img"></div>
					
						<p class="img-desc" >
							<?php the_title(); ?>
						</p>
					</div>
				</a>

			</div>

		<?php endwhile; ?>

	</div>
<div class="row">

	<div class="col-sm-12">
		<div class="paginacao-estilo1">
			<?php get_template_part('pagination'); ?>
		</div>
	</div>
</div>
</div>

<?php else: ?>

	<!-- article -->
	<article>
		<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
		<a href="<?php echo home_url("/");?>">< Voltar</a>
	</article>
	<!-- /article -->

<?php endif; ?>
