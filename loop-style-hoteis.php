
<?php


global $post;

$cats = array();
foreach (get_the_category($post_id) as $c) {
	$cat = get_category($c);
	array_push($cats, $cat->cat_ID);
}

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$currentID = get_the_ID();

if (has_children()) {
	$config = array( 
		'post_type' => 'page',
		'posts_per_page' => 6,
		'post_parent' =>  $post->ID,
		'paged' => $paged,
		'order' => 'ASC',
	);
}

else {
	$config = array( 
		'post_type' => 'hotel',
		'posts_per_page' => 6,
		'category__in' =>  $cats,
		'paged' => $paged,
		'order' => 'ASC',
	);
}



$query = new WP_Query( $config );


?>


<div class="row">
	<div class="col-sm-12">
		<h1><?php the_title(); ?></h1>
	</div>
	<div class="col-sm-12">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<?php the_content(); 


			?>

		<?php endwhile; endif; 


		global $wp_query;
		$wp_query = $query;

		?>
	</div>


</div>
<div class="box-elems-paginacao">
	
	<?php posts_nav_link(' ','<div class="btn-pag btn-page-prev">prev</div>','<div class="btn-pag btn-page-next">next</div>'); ?>

	<div class="row">

		<?php 


		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {
				$query->the_post();

				$url_post = get_the_post_thumbnail_url($query->post->ID,'full');

				if ($url_post == "") {
					$url_post = "https://placehold.it/313x210&text=Not%20Thumbnail%20313x210px";
				}


				?>


				<div class="col-sm-4">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<div class="box-img-desc" style="background-image: url(<?php echo $url_post; ?>)">
							<p class="img-desc" >
								<?php the_title(); ?>
							</p>
						</div>
					</a>

				</div>

				<?php

			};
			wp_reset_postdata();
		}

		else {
			?>

			<div class="col-sm-12">
				<h2 class="text-danger" style="margin-top: -21px; ">
					Nenhum resultado encontrado...
				</h2>
			</div>
			<?php
		}



		?>

	</div>
	<div class="row">
		
		<div class="col-sm-6 pull-right">
			<div class="paginacao-estilo1">

				<div class="pagination">
					<?php 

					get_template_part('pagination');
					?>
				</div>
			</div>
		</div>
	</div>

</div>


