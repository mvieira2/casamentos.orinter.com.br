<?php get_header(); ?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">

<main class="center" role="main">
	<!-- section -->
	<section>

		<h1><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo " ".get_search_query(); ?></h1>

		<?php get_sidebar(); ?>

		<?php get_template_part('loop-style-3'); ?>

		<?php //get_template_part('pagination'); ?>

	</section>
	<!-- /section -->
</main>

<style>
	main form.search {
    margin: auto;
}

main input.search-input {
    border: 1px solid #e5e5e5;
}
</style>

<?php get_footer(); ?>
