<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.jpg" rel="shortcut icon">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.jpg" rel="apple-touch-icon-precomposed">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<?php wp_head(); ?>
	<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
        	assets: '<?php echo get_template_directory_uri(); ?>',
        	tests: {}
        });
    </script>

</head>
<body <?php body_class('fadeIn'); ?> ng-app="meuApp">


	
	<header>
		
		<div class="header header-fixo-disabled">

			<div class="center">
				<nav class="navbar">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"  data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar linha1"></span>
								<span class="icon-bar linha2"></span>
								<span class="icon-bar linha3"></span>                        
							</button>
							<a class="navbar-brand" href="http://weddings.orinter.com.br">
								<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="logo">
							</a>
						</div>
						<div class="collapse navbar-collapse comeca-menu" id="myNavbar">
							<div class="sombra-links"></div>
							<?php html5blank_nav(); ?>
						</div>
					</div>
				</nav>
			</div>
			


			
		</div>



	</header>


