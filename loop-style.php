
<div class="row">
	<div class="col-sm-6">
		<h1><?php _e( get_field("tipo"), 'html5blank' ); ?></h1>
	</div>
	<div class="col-sm-6">
		<div class="paginacao-estilo1">
			<?php get_template_part('pagination'); ?>
		</div>
	</div>
</div>
<div class="box-elems-paginacao">

	<?php posts_nav_link(' ','<div class="btn-pag btn-page-prev">prev</div>','<div class="btn-pag btn-page-next">next</div>'); ?>

	<div class="row">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<div class="col-sm-4">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<div class="box-img-desc logo-bg" style="background-image: url(<?php echo get_field("logo_da_cia"); ?>)">
						<p class="img-desc" >
							<?php the_title(); ?>
						</p>
					</div>
				</a>

			</div>

		<?php endwhile; ?>

	</div>

</div>

<?php else: ?>

	<!-- article -->
	<article>
		<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
		<a href="<?php echo home_url("index.php/cias");?>">< Voltar</a>
	</article>
	<!-- /article -->

<?php endif; ?>
