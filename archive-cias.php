<?php get_header(); ?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">

<main class="center" role="main">
	<!-- section -->
	<section>

		<div class="row <?php echo isset($_GET['tipo']) ? 'hide' : ''; ?>">
			<div class="col-sm-12">
				<h1><?php _e( 'Companhias Marítimas', 'html5blank' ); ?>
					

				</h1>
			</div>
			<div class="col-sm-4">
				<a href="?tipo=maritimo">
					<div class="box-img-desc" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/maritimo.jpg)">
						<p class="img-desc">
							<p class="img-desc">
								Marítimo
							</p>
						</div>
					</a>
				</div>

				<div class="col-sm-4">
					<a href="?tipo=Fluvial">
						<div class="box-img-desc" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/fluvial.jpg)">
							<p class="img-desc">
								<p class="img-desc">
									Fluvial
								</p>
							</div>
						</a>
					</div>
					<div class="col-sm-4">
						<a href="?tipo=Expedicao">
							<div class="box-img-desc" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/expedicao.jpg)">
								<p class="img-desc">
									Expedição
								</p>
							</div>
						</a>
					</div>


				</div>


				<?php

				if (isset($_GET['tipo'])) {
					get_template_part('loop-style');
				}

				?>



			</section>
			<!-- /section -->
			<?php //get_sidebar(); ?>
		</main>



		<?php get_footer(); ?>
