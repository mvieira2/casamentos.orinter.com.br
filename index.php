<?php get_header();


$all_logos_cias_url = get_posts(array(
	'post_type'		=> 'cias',
	'fields'          => 'ids', 
	'posts_per_page'  => -1
));

$all_logos_cias_url = array_map(function($arr){
	return array('logo' => get_field('logo_da_cia', $arr), 'url' => get_the_permalink($arr) );
},$all_logos_cias_url);



?>
<div class="margin-top-header sliders slider-1" >

	<?php

	$page_ID = get_page_by_path('home')->ID;

	if( have_rows('banners', $page_ID) ):


		while ( have_rows('banners', $page_ID) ) : the_row();

			?>


			<div >
				<a href="<?php the_sub_field('link_do_banner', $page_ID); ?>">
					<img class="sliderImagens no-ar" src="<?php the_sub_field('imagem_url', $page_ID); ?>" alt="">
				</a>
			</div>



			<?php


		endwhile;

	else :

		?>
		<div >
			<a href="">
				<img class="sliderImagens" src="<?php echo get_template_directory_uri(); ?>/img/banner_principal.jpg?v1" alt="">
			</a>
		</div>

		<?php
	endif;

	?>



</div>

<script>

	(function ($, root, undefined) {
		$(".slider-1").slick({
			slidesToShow: 1, 
			infinite: true, 
			autoplay: true, 
			fade : true, 
			speed: 300, 
			cssEase:"linear", 
			adaptiveHeight: true, 
		prevArrow: '<button type="button" class="my-slick-prev"><img class="sliderImagens" src="<?php echo get_template_directory_uri(); ?>/img/top-slider-nav-left.png" alt=""></button>',
			nextArrow: '<button type="button" class="my-slick-next"><img class="sliderImagens" src="<?php echo get_template_directory_uri(); ?>/img/top-slider-nav-right.png" alt=""></button>',
			dots:true
		}); 
	})(jQuery, this);



	
</script>


<main class="center" role="main">
	<!-- section -->
	<section class="">

		<div class="row">
			<div class="col-sm-12 text-center">
				<h2>DESTINOS</h2>
			</div>



			<div class="col-sm-12 ">
				
				<div class="lista-alinhada" style="width: 100%;"> 


					<div class="row">

						<?php


						$config = array( 
							'post_type' => 'page',
							'posts_per_page' => -1,
							'order' => 'ASC',
						) ;

						$descendants = new WP_Query( $config );
						$excl = '';

						if ( $descendants->have_posts() ) {

							while ( $descendants->have_posts() ) {
								$descendants->the_post();
								if (has_category( "lgbti", $descendants->post->ID) ) {
									$excl .= $descendants->post->ID . ",";
								}
							}

							wp_reset_postdata();
						}

						$children = get_pages(array(
							'child_of' => get_page_by_path("destinos")->ID, 
							'parent' => get_page_by_path("destinos")->ID,
							//'exclude'  => $excl,
						));


						$colunas = 0;
						$media_num_colunas = 4;		

						if (get_field('limite_de_paises_por_coluna',get_page_by_path('home')->ID)) {
							$limite_linha = get_field('limite_de_paises_por_coluna',get_page_by_path('home')->ID);
						}	


						//$limite_linha = 7;


						foreach ($children as $key => $child) { 
							$colunas = $colunas + 1;

							$children_1 = get_pages(array(
								'child_of' => $child->ID, 
								'parent' => $child->ID,
								//'exclude'  => $excl,
							));

							if (count($children_1) > 0) {

								?>

								<?php if ($colunas == 1 or count($children_1) > $limite_linha ) { ?>
									
									<?php if ($colunas > 1 && count($children_1) > $limite_linha ) { ?>

									</div>

								<?php } ?>

								<div class="col-sm-3">

								<?php } ?>
								<div>

									<h2 class="title_lista"><?php echo $child->post_title ; ?></h2>

									<ul class="children">


										<?php

										foreach ($children_1 as $child_1) { 
											?>
											<li class="page_item">
												<a href="<?php echo get_permalink($child_1->ID); ?>"><?php echo $child_1->post_title; ?></a>
											</li>

										<?php } ?>

									</ul>

									
								</div>

								<?php if ($colunas == $media_num_colunas or count($children_1) > $limite_linha ) { ?>
								</div>
								<?php 
								$colunas = 0;
							}; 
						}; 

					}; ?>

				</div>


			</div>

		</div>


	</section>
	<!-- /section -->

	<?php //get_sidebar(); ?>
</main>


<?php get_footer(); ?>
