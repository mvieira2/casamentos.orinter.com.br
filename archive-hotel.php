<?php get_header(); ?>

<img class="banner-unico-topo" src="<?php echo get_template_directory_uri(); ?>/img/banner_topo.jpg" alt="">

<main class="center" role="main">
	<!-- section -->
	<section>

		<h1>Hotéis</h1>

		<?php get_template_part('loop-style-3'); ?>

		<?php //get_template_part('pagination'); ?>

	</section>
	<!-- /section -->
</main>

<style>
	main form.search {
    margin: auto;
}

main input.search-input {
    border: 1px solid #e5e5e5;
}
</style>

<?php get_footer(); ?>
